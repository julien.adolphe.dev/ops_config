from django.test import TestCase
from .models import User
# Create your tests here.


class UserTestCase(TestCase):
    @classmethod
    def setup_data(cls):
        cls.user = User.objects.create(name='Casimir', email='casimir@gmail.com')

    def test_email_type(self):
        self.assertIsInstance(self.user.email, str)

    def test_name_length(self):
        self.assertNotIsInstance(self.user.name, int)

