from rest_framework import routers
from .api import UserViewSet

router = routers.DefaultRouter()
router.register(r'api/api', UserViewSet, 'api')

urlpatterns = router.urls
