## API SETUP

**It is suggested to use a virtual environment like virtualenv :** 

_pip3 install virtualenv_

_cd djangoProject_

_source venv/bin/activate_
***
**Install the following packages with pip :**

_pip3 install django==3.0.5 djangorestframework djongo sqlparse==0.2.4_
***
**Make the database migrations :**

_python3 manage.py makemigrations <app>_

_python3 manage.py migrate_
***
**Launch the server :**

_cd djangoProject/api_project_

_python3 manage.py runserver_
***
**Running the tests :**

_./manage.py test_
***

